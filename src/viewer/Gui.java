package viewer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gui extends JFrame {
	private JButton greenBT;
	private JButton redBT;
	private JButton blueBT;
	private JPanel backgroundPane;
	private JRadioButton colorRedRadioBt;
	private JRadioButton colorGreenRadioBt;
	private JRadioButton colorBlueRadioBt;
	private JComboBox<String> colorCombo;
	private int red;
	private int green;
	private int blue;
	private JCheckBox colorRedCheck;
	private JCheckBox colorGreenCheck;
	private JCheckBox colorBlueCheck;
	private JTextField depositField;
	private JTextField withdrawField;
	private JLabel balanceLabel;
	private JButton depositBt;
	private JButton withdrawBt;
	private JTextArea balanceArea;

	public Gui() {
		// TODO Auto-generated constructor stub
//		createFrame();
//		createFrame1();
	}
	public void createFrame() {
		// TODO Auto-generated method stub
		redBT = new JButton("Red");
		greenBT = new JButton("Green");
		blueBT = new JButton("Blue");
		JPanel btPane = new JPanel();
		btPane.add(redBT);
		btPane.add(greenBT);
		btPane.add(blueBT);
		add(btPane,BorderLayout.SOUTH);
		backgroundPane = new JPanel();
		add(backgroundPane,BorderLayout.CENTER);
		
		redBT.addActionListener(new RedBtListener());
		greenBT.addActionListener(new GreenBtListener());
		blueBT.addActionListener(new BlueBtListener());
		
	}
	public void createFrame1(){
		colorRedRadioBt = new JRadioButton("Red");
		colorGreenRadioBt = new JRadioButton("Green");
		colorBlueRadioBt = new JRadioButton("Blue");
		ButtonGroup colorGroup = new ButtonGroup();
		colorGroup.add(colorRedRadioBt);
		colorGroup.add(colorGreenRadioBt);
		colorGroup.add(colorBlueRadioBt);
		JPanel radioPane = new JPanel();
		radioPane.add(colorRedRadioBt);
		radioPane.add(colorGreenRadioBt);
		radioPane.add(colorBlueRadioBt);
		add(radioPane,BorderLayout.SOUTH);
		backgroundPane = new JPanel();
		add(backgroundPane,BorderLayout.CENTER);
		colorRedRadioBt.addActionListener(new RedBtListener());
		colorGreenRadioBt.addActionListener(new GreenBtListener());
		colorBlueRadioBt.addActionListener(new BlueBtListener());
	}
	public void createFrame2(){
		colorCombo = new JComboBox<String>();
		colorCombo.addItem("Red");
		colorCombo.addItem("Green");
		colorCombo.addItem("Blue");
		JPanel comboPane = new JPanel();
		comboPane.add(colorCombo);
		add(comboPane,BorderLayout.SOUTH);
		backgroundPane = new JPanel();
		add(backgroundPane,BorderLayout.CENTER);
		colorCombo.addActionListener(new ComboBoxListener());
		colorCombo.setSelectedIndex(0);
		
	}
	public void createFrame3(){
		colorRedCheck = new JCheckBox("Red");
		colorGreenCheck = new JCheckBox("Green");
		colorBlueCheck = new JCheckBox("Blue");
		JPanel checkPane = new JPanel();
		checkPane.add(colorRedCheck);
		checkPane.add(colorGreenCheck);
		checkPane.add(colorBlueCheck);
		add(checkPane,BorderLayout.SOUTH);
		backgroundPane = new JPanel();
		add(backgroundPane,BorderLayout.CENTER);
		colorRedCheck.addActionListener(new RedCheckListener());
		colorGreenCheck.addActionListener(new GreenCheckListener());
		colorBlueCheck.addActionListener(new BlueCheckListener());
		
	}
	public void createFrame4(){
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Colors");
		JMenuItem redMenu = new JMenuItem("Red");
		JMenuItem greenMenu = new JMenuItem("Green");
		JMenuItem blueMenu = new JMenuItem("Blue");
		menuBar.add(menu);
		menu.add(redMenu);
		menu.add(greenMenu);
		menu.add(blueMenu);
		add(menuBar,BorderLayout.NORTH);
		backgroundPane = new JPanel();
		add(backgroundPane,BorderLayout.CENTER);
		redMenu.addActionListener(new RedBtListener());
		greenMenu.addActionListener(new GreenBtListener());
		blueMenu.addActionListener(new BlueBtListener());
	}
	public void createFrame5(){
		JLabel depositLabel = new JLabel("Deposit");
		JLabel withdrawlabel = new JLabel("Withdraw");
		depositField = new JTextField(10);
		withdrawField = new JTextField(10);
		depositBt = new JButton("Deposit");
		withdrawBt = new JButton("Withdraw");
		balanceLabel = new JLabel("Balance : 0.0");
		JPanel depositPane = new JPanel();
//		depositPane.setLayout(new GridLayout(3, 1));
		depositPane.add(depositLabel);
		depositPane.add(depositField);
		depositPane.add(depositBt);
		JPanel withdrawPane = new JPanel();
//		withdrawPane.setLayout(new GridLayout(3,1));
		withdrawPane.add(withdrawlabel);
		withdrawPane.add(withdrawField);
		withdrawPane.add(withdrawBt);
		add(depositPane,BorderLayout.WEST);
		add(withdrawPane,BorderLayout.EAST);
		add(balanceLabel,BorderLayout.SOUTH);
	}
	public void createFrame6(){
		JLabel depositLabel = new JLabel("Deposit");
		JLabel withdrawlabel = new JLabel("Withdraw");
		depositField = new JTextField(10);
		withdrawField = new JTextField(10);
		depositBt = new JButton("Deposit");
		withdrawBt = new JButton("Withdraw");
//		balanceLabel = new JLabel("Balance : 0.0");
		balanceArea = new JTextArea("Detail :");
		JScrollPane balanceScroll = new JScrollPane(balanceArea);
		JPanel depositPane = new JPanel();
//		depositPane.setLayout(new GridLayout(3, 1));
		depositPane.add(depositLabel);
		depositPane.add(depositField);
		depositPane.add(depositBt);
		JPanel withdrawPane = new JPanel();
//		withdrawPane.setLayout(new GridLayout(3,1));
		withdrawPane.add(withdrawlabel);
		withdrawPane.add(withdrawField);
		withdrawPane.add(withdrawBt);
		JPanel bankPane = new JPanel();
		bankPane.setLayout(new GridLayout(1,2));
		bankPane.add(depositPane);
		bankPane.add(withdrawPane);
//		add(depositPane,BorderLayout.WEST);
//		add(withdrawPane,BorderLayout.EAST);
		add(bankPane,BorderLayout.NORTH);
		add(balanceScroll,BorderLayout.CENTER);
	}
	class RedBtListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			setColorPane(Color.RED);
		}
		
	}
	class GreenBtListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			setColorPane(Color.GREEN);
		}
		
	}
	class BlueBtListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			setColorPane(Color.BLUE);
		}
		
	}
	class ComboBoxListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int index = colorCombo.getSelectedIndex();
			if(index==0){
				setColorPane(Color.RED);
			}
			else if(index==1){
				setColorPane(Color.GREEN);
			}
			else{
				setColorPane(Color.BLUE);
			}
			
		}
		
	}
	class RedCheckListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(colorRedCheck.isSelected() == true){
				red = 225;
			}
			else{
				red = 0;
			}
			setColorPane(new Color(red,green,blue));
		}
		
	}
	class GreenCheckListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(colorGreenCheck.isSelected() == true){
				green = 225;
			}
			else{
				green = 0;
			}
			setColorPane(new Color(red,green,blue));
		}
		
	}
	class BlueCheckListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(colorBlueCheck.isSelected() == true){
				blue = 225;
			}
			else{
				blue = 0;
			}
			setColorPane(new Color(red,green,blue));
		}
	
	}
	public void setColorPane(Color color){
		backgroundPane.setBackground(color);
	}
	public void addDepositListener(ActionListener list){
		depositBt.addActionListener(list);
	}
	public void addWithdrawListeneer(ActionListener list){
		withdrawBt.addActionListener(list);
	}
	public int getDepositField(){
		String price = depositField.getText();
		int amount = Integer.parseInt(price);
		depositField.setText("0");
		return amount;
	}
	public int getWithdrawField(){
		String price = withdrawField.getText();
		int amount = Integer.parseInt(price);
		withdrawField.setText("0");
		return amount;
	}
	public void setBalance(String str){
		balanceLabel.setText(str);
	}
	public void setBalanceArea(String str){
		String str1 = balanceArea.getText();
		balanceArea.setText(str1+"\n"+str);
	}

}
