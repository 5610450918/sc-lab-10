package model;

public class BankAccount {
	private String name;
	private String id;
	private int balance;
	public BankAccount(String id,String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.id = id;
	}
	public void deposit(int amount){
		balance += amount;
	}
	public void withdraw(int amount){
		balance -= amount;
	}
	public long checkBalance(){
		return balance;
	}
	public String getName(){
		return name;
	}
	public String getId(){
		return id;
	}

}
