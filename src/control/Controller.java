package control;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;
import viewer.Gui;

public class Controller {
	private Gui frame1 ;
	private Gui frame2 ;
	private Gui frame3 ;
	private Gui frame4 ;
	private Gui frame5 ;
	private Gui frame6;
	private Gui frame7;
	private BankAccount account;
	private BankAccount account1;

	public Controller() {
		// TODO Auto-generated constructor stub
		frame1 = new Gui();
		frame2 = new Gui();
		frame3 = new Gui();
		frame4 = new Gui();
		frame5 = new Gui();
		frame6 = new Gui();
		frame7 = new Gui();
		frame1.createFrame();
		frame1.setSize(250, 350);
		frame1.setVisible(true);
		frame2.createFrame1();
		frame2.setSize(250, 350);
		frame2.setVisible(true);
		frame2.setLocation(50, 50);
		frame3.createFrame2();
		frame3.setSize(250, 350);
		frame3.setVisible(true);
		frame3.setLocation(100, 70);
		frame4.createFrame3();
		frame4.setSize(250, 350);
		frame4.setVisible(true);
		frame4.setLocation(130, 100);
		frame5.createFrame4();
		frame5.setSize(250, 350);
		frame5.setVisible(true);
		frame5.setLocation(160, 130);
		frame6.createFrame5();
		frame6.setSize(550, 100);
		frame6.setVisible(true);
		frame6.setLocation(190, 150);
		frame6.addDepositListener(new DepositBtListener());
		frame6.addWithdrawListeneer(new WithdrawBtListener());
		frame7.createFrame6();
		frame7.setSize(600, 350);
		frame7.setVisible(true);
		frame7.setLocation(210, 190);
		frame7.addDepositListener(new Deposit2BtListener());
		frame7.addWithdrawListeneer(new Withdraw2BtListener());
		testCase();
//		frame.createFrame1();
//		frame.setRedListener(new RedBtListener());
//		frame.setGreenListener(new GreenBtListener());
//		frame.setBlueListener(new BlueBtListener());
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}
	public void testCase(){
		account = new BankAccount("0001","Boss");
		account1 = new BankAccount("0002","Sand");
		
	}
	class DepositBtListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int price = frame6.getDepositField();
			account.deposit(price);
			frame6.setBalance("Balance : "+account.checkBalance());
			
		}
		
	}
	class WithdrawBtListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int price = frame6.getWithdrawField();
			account.withdraw(price);
			frame6.setBalance("Balance : "+account.checkBalance());
			
		}
		
	}
	class Deposit2BtListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int price = frame7.getDepositField();
			account1.deposit(price);
			frame7.setBalanceArea("Deposit : "+price);
			frame7.setBalanceArea("Balance : "+account1.checkBalance()+"\n");
			
		}
		
	}
	class Withdraw2BtListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int price = frame7.getWithdrawField();
			account1.withdraw(price);
			frame7.setBalanceArea("Withdraw : "+price);
			frame7.setBalanceArea("Balance : "+account1.checkBalance()+"\n");
			
		}
		
	}
}
